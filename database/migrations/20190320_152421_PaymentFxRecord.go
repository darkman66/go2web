package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type PaymentFxRecord_20190320_152421 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &PaymentFxRecord_20190320_152421{}
	m.Created = "20190320_152421"

	migration.Register("PaymentFxRecord_20190320_152421", m)
}

// Run the migrations
func (m *PaymentFxRecord_20190320_152421) Up() {
	m.SQL(`CREATE TABLE IF NOT EXISTS payment_fx_record(
		id integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
        contract_reference varchar(255) NOT NULL DEFAULT '' ,
        exchange_rate varchar(255) NOT NULL DEFAULT '' ,
        original_amount varchar(255) NOT NULL DEFAULT '' ,
        original_currency varchar(255) NOT NULL DEFAULT ''
	)`)
	m.SQL(`CREATE INDEX idx_payment_fx_record
		ON payment_fx_record (contract_reference)`)
}

// Reverse the migrations
func (m *PaymentFxRecord_20190320_152421) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update

}
