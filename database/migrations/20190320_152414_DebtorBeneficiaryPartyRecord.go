package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type DebtorBeneficiaryPartyRecord_20190320_152414 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &DebtorBeneficiaryPartyRecord_20190320_152414{}
	m.Created = "20190320_152414"

	migration.Register("DebtorBeneficiaryPartyRecord_20190320_152414", m)
}

// Run the migrations
func (m *DebtorBeneficiaryPartyRecord_20190320_152414) Up() {
	m.SQL(`CREATE TABLE IF NOT EXISTS debtor_beneficiary_party(
        id integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
        account_name varchar(255) NOT NULL DEFAULT '' ,
        account_number varchar(255) NOT NULL DEFAULT '' ,
        account_number_code varchar(255) NOT NULL DEFAULT '' ,
        account_type integer NOT NULL DEFAULT 0 ,
        address varchar(255) NOT NULL DEFAULT '' ,
        bank_i_d varchar(255) NOT NULL DEFAULT '' ,
        bank_i_d_code varchar(255) NOT NULL DEFAULT '' ,
        name varchar(255) NOT NULL DEFAULT '')
	`)
	m.SQL(`CREATE INDEX idx_debtor_beneficiary_party
		ON debtor_beneficiary_party (account_number, account_number_code)`)
}

// Reverse the migrations
func (m *DebtorBeneficiaryPartyRecord_20190320_152414) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update

}
