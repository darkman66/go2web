package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type PaymentRecord_20190320_152408 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &PaymentRecord_20190320_152408{}
	m.Created = "20190320_152408"

	migration.Register("PaymentRecord_20190320_152408", m)
}

// Run the migrations
func (m *PaymentRecord_20190320_152408) Up() {
	m.SQL(`CREATE TABLE IF NOT EXISTS payment_record (
		id INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
        payment_record varchar(255) NOT NULL DEFAULT '' ,
        amount varchar(255) NOT NULL DEFAULT '' ,
        currency varchar(255) NOT NULL DEFAULT '' ,
        end_to_end_reference varchar(255) NOT NULL DEFAULT '' ,
        numeric_reference varchar(255) NOT NULL DEFAULT '' ,
        payment_id varchar(255) NOT NULL DEFAULT '' ,
        payment_purpose varchar(255) NOT NULL DEFAULT '' ,
        payment_scheme varchar(255) NOT NULL DEFAULT '' ,
        payment_type varchar(255) NOT NULL DEFAULT '' ,
        processing_date date NOT NULL,
        reference varchar(255) NOT NULL DEFAULT '' ,
        scheme_payment_subype varchar(255) NOT NULL DEFAULT '' ,
        scheme_payment_type varchar(255) NOT NULL DEFAULT '' ,
        debtor_party_id integer,
        beneficiary_party_id integer,
        payment_fx_id integer,
        charges_information_id integer
    )`)
    m.SQL(`CREATE INDEX payment_record_payment_record ON payment_record (payment_record)`)
	m.SQL(`CREATE UNIQUE INDEX  payment_record_idx ON
		payment_record (payment_record, payment_type, payment_scheme, payment_id)`)
	m.SQL(`CREATE INDEX  payment_record_idx2 ON
		payment_record (debtor_party_id, beneficiary_party_id, payment_fx_id, charges_information_id)`)

}

// Reverse the migrations
func (m *PaymentRecord_20190320_152408) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update

}
