package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type ChargesInformationRecord_20190320_152429 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &ChargesInformationRecord_20190320_152429{}
	m.Created = "20190320_152429"

	migration.Register("ChargesInformationRecord_20190320_152429", m)
}

// Run the migrations
func (m *ChargesInformationRecord_20190320_152429) Up() {
	m.SQL(`CREATE TABLE IF NOT EXISTS charges_information(
		id integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
        bearer_code varchar(255) NOT NULL DEFAULT '' ,
        receiver_charges_amount varchar(255) NOT NULL DEFAULT '' ,
        receiver_charges_currency varchar(255) NOT NULL DEFAULT ''
	)`)
	m.SQL(`CREATE INDEX idx_charges_information
		ON charges_information (bearer_code)`)
}

// Reverse the migrations
func (m *ChargesInformationRecord_20190320_152429) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update

}
