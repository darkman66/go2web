package main

import (
	"github.com/astaxie/beego/migration"
)

// DO NOT MODIFY
type AmountRecord_20190320_152425 struct {
	migration.Migration
}

// DO NOT MODIFY
func init() {
	m := &AmountRecord_20190320_152425{}
	m.Created = "20190320_152425"

	migration.Register("AmountRecord_20190320_152425", m)
}

// Run the migrations
func (m *AmountRecord_20190320_152425) Up() {
	m.SQL(`CREATE TABLE IF NOT EXISTS amount(
		id integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
        amount varchar(255) NOT NULL DEFAULT '' ,
        currency varchar(255) NOT NULL DEFAULT '' ,
        charges_information_record_id integer
	)`)
	m.SQL(`CREATE INDEX idx_amount
		ON amount (charges_information_record_id)`)
}

// Reverse the migrations
func (m *AmountRecord_20190320_152425) Down() {
	// use m.SQL("DROP TABLE ...") to reverse schema update

}
