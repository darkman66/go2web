package routers

import (
	"gitlab.com/darkman66/go2web/controllers"
	"github.com/astaxie/beego"
)

func init() {
    beego.Router("/", &controllers.MainController{})
    beego.Router("/api/payments/attribute/:payment_hash([0-9-a-f]+)/", &controllers.PaymentAttributeDetailsController{})
    beego.Router("/api/payments/attribute/:payment_hash([0-9-a-f]+)/sponsor_party/", &controllers.PaymentSponsoredListController{})
    beego.Router("/api/payments/attribute/:payment_hash([0-9-a-f]+)/sponsor_party/:resource_id([0-9-a-f]+)/", &controllers.PaymentSponsoredDetailsController{})
    beego.Router("/api/payments/attribute/", &controllers.PaymentAttributeListController{})
    beego.Router("/api/payments/", &controllers.PaymentListController{})
    beego.Router("/api/payments/fetch/", &controllers.FetchData{})
}
