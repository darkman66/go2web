# go 2 web

Example of using [Go Lang](http://golang.org) as RESTFUL-API with full support for CRUD operations.

# Frameworks

## User interface

* [Bootstrap](https://getbootstrap.com)
* [bootstrap UI tweaks](https://mdbootstrap.com)

## Javascript for UI manipulations

* [React](http://reactjs.org) - makes JS life easier

## Building Javascript

* [NPM](http://npmjs.com)
* [Webpack](https://webpack.js.org)
* [Babel - javascript compiler](https://babeljs.io)


## Go

This is the main framework to server web content and REST-API

* [BeeGo](http://beego.me) - main web framework with full MVC suport
* [DateParse](https://github.com/araddon/dateparse) - Parse many date strings without knowing format in advance
* [UUID package](https://github.com/satori/go.uuid) - Go implementation of Universally Unique Identifier (UUID)
* [Generic data validate](https://github.com/gookit/validate) - so much missed validator
* [Linter](https://godoc.org/golang.org/x/lint/golint) - so your code looks beautiful
* [DB Fixtures](https://github.com/go-testfixtures/testfixtures/tree/v2.5.3) - to load data smarter for testing

## Automations and building

* [Guard](https://github.com/guard/guard) - automation of building and testing
