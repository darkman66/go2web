PROJECT_NAME := "go2web"
PKG := "gitlab.com/darkman66/$(PROJECT_NAME)"
PKG_LIST := "go2web/..."
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)

.PHONY: all dep build clean test coverage coverhtml lint

all: build

lint: dep ## Lint the files
	@golint -set_exit_status ${PKG_LIST}

test: ## Run unittests
	@go test -v `go list ./... | grep -v migrations`

test_local: ## Run unittests
	@mysql -u root -e "create database IF NOT EXISTS nee_test"
	@go test -v `go list ./... | grep -v migrations`


race: dep ## Run data race detector
	@go test -race -short ${PKG_LIST}

msan: dep ## Run memory sanitizer
	@go test -msan -short ${PKG_LIST}

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

dep: ## Get the dependencies
	@go get -v -u golang.org/x/lint/golint
	@go get -v "github.com/smartystreets/goconvey/convey"
	@go get -v -d ./... ./tests/

build: dep ## Build the binary file
	@go build -i -v ${PKG}

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME)

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
