package main

import (
	"os"
	"fmt"
	"database/sql"
	_ "gitlab.com/darkman66/go2web/routers"
	log "github.com/goinggo/tracelog"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/testfixtures.v2"
)


var (
    db *sql.DB
    fixtures *testfixtures.Context
)


func getenv(key, default_value string) string {
    value := os.Getenv(key)
    if len(value) == 0 {
        return default_value
    }
    return value
}


func main() {
	// var fixtures *testfixtures.Context
	var err error
	var connectionString string

	log.Start(log.LevelTrace)
	log.Started("main", "Register db driver")
    orm.RegisterDriver("mysql", orm.DRMySQL)

	mysqlPassword := getenv("MYSQL_PASSWORD", "") // for gitlab only :)
	mysqlHost := getenv("MYSQL_HOST", "127.0.0.1") // for gitlab only :)
	mysqlUser := getenv("MYSQL_USER", "root")
	mysqlDb := getenv("MYSQL_DB", "nee")
	connectionString = fmt.Sprintf("%s:%s@tcp(%s:3306)/%s?charset=utf8", mysqlUser, mysqlPassword, mysqlHost, mysqlDb)

    orm.RegisterDataBase("default", "mysql", connectionString)

	// let's laod the show
	db, err = orm.GetDB()
	if err != nil {
	    log.Errorf(err, "main", "main", "Getting default DB failed!")
	}


	err = testfixtures.GenerateFixtures(db, &testfixtures.MySQL{},
	    "./fixtures/",
	)

	if err != nil {
	    log.Errorf(err, "main", "main", "Dumping fixtures failed! %v")
	}
}
