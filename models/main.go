package models

import (
	"time"
	"github.com/astaxie/beego/orm"
)


type AmountRecord struct {
	ID int `orm:"column(id);pk;auto"`
    Amount string
    Currency string `json:"currency"`
    ChargesInformationRecordID int `orm:"column(charges_information_record_id)"`// *ChargesInformationRecord `orm:"null;rel(fk);"`
}


type ChargesInformationRecord struct {
	ID int `orm:"column(id);pk;auto"`
    BearerCode string
    SenderCharges []*AmountRecord `orm:"-"`
    ReceiverChargesAmount string
    ReceiverChargesCurrency string
}


type PaymentFxRecord struct {
	ID int `orm:"column(id);pk;auto"`
    ContractReference string
    ExchangeRate string
    OriginalAmount string
    OriginalCurrency string
}


type DebtorBeneficiaryPartyRecord struct {
	ID int `orm:"column(id);pk;auto"`
    AccountName string
    AccountNumber string
    AccountNumberCode string
    AccountType int
    Address string
    BankID string
    BankIDCode string
    Name string
}


type PaymentRecord struct {
    ID int `orm:"column(id);pk;auto"`
    PaymentRecord string `orm:"column(payment_record);index"`
    Amount string `orm:"column(amount)"`
    Currency string `orm:"column(currency)"`
    EndToEndReference string `orm:"column(end_to_end_reference)"`
    NumericReference string `orm:"column(numeric_reference)"`
    PaymentID string `orm:"column(payment_id)"`
    PaymentPurpose string `orm:"column(payment_purpose)"`
    PaymentScheme string `orm:"column(payment_scheme)"`
    PaymentType string `orm:"column(payment_type)"`
    ProcessingDate time.Time `orm:"auto_now_add;type(date);column(processing_date)"`
    Reference string `orm:"column(reference)"`
    SchemePaymentSubype string `orm:"column(scheme_payment_subype)"`
    SchemePaymentType string `orm:"column(scheme_payment_type)"`
    DebtorParty *DebtorBeneficiaryPartyRecord `orm:"null;rel(fk)"`
    BeneficiaryParty *DebtorBeneficiaryPartyRecord `orm:"null;rel(fk);"`
    PaymentFx *PaymentFxRecord `orm:"null;rel(fk)"`
    ChargesInformation *ChargesInformationRecord `orm:"null;rel(fk)"`
}


func (payment_record *PaymentRecord) TableName() string {
	// just to demo mapping so that table name can be different than model name
    return "payment_record"
}


func (debtor_beneficiary_party *DebtorBeneficiaryPartyRecord) TableName() string {
    return "debtor_beneficiary_party"
}


func (payment_fx_record *PaymentFxRecord) TableName() string {
    return "payment_fx_record"
}


func (charges_information *ChargesInformationRecord) TableName() string {
    return "charges_information"
}


func (amount *AmountRecord) TableName() string {
    return "amount"
}


func init() {
    // Need to register model in init
    orm.RegisterModel(new(PaymentRecord))
    orm.RegisterModel(new(DebtorBeneficiaryPartyRecord))
    orm.RegisterModel(new(PaymentFxRecord))
    orm.RegisterModel(new(AmountRecord))
    orm.RegisterModel(new(ChargesInformationRecord))
}
