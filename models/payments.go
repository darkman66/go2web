package models

/*
This is only for JSON struct storage
*/

type Amount struct {
    Amount string `json:"amount"`
    Currency string `json:"currency"`
}

type ChargesInformation struct {
    BearerCode string `json:"bearer_code"`
    SenderCharges []Amount `json:"sender_charges"`
    ReceiverChargesAmount string `json:"receiver_charges_amount"`
    ReceiverChargesCurrency string `json:"receiver_charges_currency"`
}

type DebtorBeneficiaryParty struct {
    AccountName string `json:"account_name"`
    AccountNumber string `json:"account_number"`
    AccountNumberCode string `json:"account_number_code"`
    AccountType int `json:"account_type"`
    Address string `json:"address"`
    BankID string `json:"bank_id"`
    BankIDCode string `json:"bank_id_code"`
    Name string `json:"name"`
}

type PaymentFx struct {
    ContractReference string `json:"contract_reference"`
    ExchangeRate string `json:"exchange_rate"`
    OriginalAmount string `json:"original_amount"`
    OriginalCurrency string `json:"original_currency"`
}

type PaymentSponsorParty struct {
    AccountNumber string `json:"account_number"`
    BankId string `json:"bank_id"`
    BankIdCode string `json:"bank_id_code"`
}

type PaymentAttribute struct {
    Amount string `json:"amount"`
    BeneficiaryParty DebtorBeneficiaryParty  `json:"beneficiary_party"`
    DebtorParty DebtorBeneficiaryParty `json:"debtor_party"`
    ChargesInformation ChargesInformation `json:"charges_information"`
    Currency string `json:"currency"`
    EndToEndReference string `json:"end_to_end_reference"`
    Fx PaymentFx
    NumericReference string `json:"numeric_reference"`
    PaymentID string `json:"payment_id"`
    PaymentPurpose string `json:"payment_purpose"`
    PaymentScheme string `json:"payment_scheme"`
    PaymentType string `json:"payment_type"`
    ProcessingDate string `json:"processing_date"`
    Reference string `json:"reference"`
    SchemePaymentSubype string `json:"scheme_payment_sub_type"`
    SchemePaymentType string `json:"scheme_payment_type"`
    SponsorParty PaymentSponsorParty `json:"sponsor_party"`
}

type PaymentItem struct {
    PaymentType string `json:"type"`
    ID string `json:"id"`
    Version int `json:"sponsor_party"`
    OrganisationID string `json:"organisation_id"`
    Attributes PaymentAttribute
}

type Payment struct {
    Data []PaymentItem
    Links map[string]string
}
