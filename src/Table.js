import React from "react";
import PropTypes from "prop-types";
import key from "weak-key";
import { Delete } from "./Payments";


const keys = [
        "ID",
        "PaymentRecord",
        "Amount",
        "Currency",
        "EndToEndReference",
        "NumericReference",
        "PaymentID"
    ];

const Table = ({ data, endpoint }) =>
    !data.length ? (
    <p>Nothing to show</p>
    ) : (
    <div>
      <div className="container-fluid">
        Showing <strong>{data.length} items</strong>
      </div>

      <table className="table is-striped">
        <thead className="thead-dark">
          <tr>
            {Object.entries(keys).map(el => <th key={key(el)}>{el[1]}</th>)}
            <th>actions</th>
          </tr>
        </thead>
        <tbody>
          {data.map(el => (
            <tr key={el.PaymentRecord}>
              {Object.entries(keys).map(el2 => <td key={key(el2)}>{ el[el2[1]] }</td>)}
              <td>
              <Delete objectId={ el.PaymentRecord } endpoint={ endpoint } />
              <a title="edit">&#9998;</a>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );

Table.propTypes = {
  data: PropTypes.array.isRequired
};
export default Table;
