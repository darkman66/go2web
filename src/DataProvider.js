import React, { Component } from "react";
import PropTypes from "prop-types";

class DataProvider extends Component {
  static propTypes = {
    endpoint: PropTypes.string.isRequired,
    render: PropTypes.func.isRequired
  };

  state = {
      data: [],
      loaded: false,
      placeholder: "Loading..."
    };

  constructor(props) {
    super(props);
    this.pleaseWait = this.pleaseWait.bind(this);
    this.loadTransactions = this.loadTransactions.bind(this);
  }

  loadTransactions() {
      fetch(this.props.endpoint)
      .then(response => {
        if (response.status !== 200) {
          return this.setState({ placeholder: "Something went wrong" });
        }
        return response.json();
      })
      .then(data => this.setState({ data: data, loaded: true }));
  }

  componentDidMount() {
      this.context.loadTransactions = this.loadTransactions
      this.context.placeholder = this.state.placeholder
      this.context.updateMessage = this.pleaseWait
      this.loadTransactions()

  }
  pleaseWait(message) {
    this.setState({ placeholder: message, loaded: false })
  }
  render() {
    const { data, loaded, placeholder } = this.state;
    return loaded ? this.props.render(data) : <div>{ placeholder }</div>;
  }
}

const TableDataContext = React.createContext({});
DataProvider.contextType = TableDataContext;

export { DataProvider, TableDataContext};
