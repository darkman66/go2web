import React, { Component } from "react";
import { TableDataContext }  from "./DataProvider";

class Delete extends React.Component {
  constructor(props) {
    super(props);
    this.activateDelete = this.activateDelete.bind(this);
  }

  activateDelete() {
    const requestOptions = {
      method: 'DELETE'
    };
    this.context.updateMessage(
        <div className="spinner-border" role="status">
          Please wait <span className="sr-only">Loading...</span>
        </div>
    )

    fetch(this.props.endpoint + '/' + this.props.objectId + '/', requestOptions)
      .then(response => {
        if (response.status !== 200) {
          return this.setState({ loading_data: <span className="alert-danger" role="alert">efresh failed!</span> });
        }
        return response.json();
      })
      .then(data => this.context.loadTransactions());

  }

  render() {
    return (
        <button title="delete" onClick={this.activateDelete}>&#128465;</button>
    );
  }
}


class Payments extends Component {
  constructor(props) {
    super(props);
    this.state = {loading_data: ''};
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    fetch(this.props.endpoint)
      .then(response => {
        if (response.status !== 200) {
          return this.setState({ loading_data: <span className="alert-danger" role="alert">Refresh failed!</span> });
        }
        return response.json();
      })
      .then(data => {
          this.setState({ loading_data: <span className="alert-success" role="alert">Status: { data.response}</span>  })
          this.context.loadTransactions()
      });
  }

  render() {
    return (
      <div>
          <span><button type="button" className="btn btn-primary btn-sm" onClick={this.handleClick}>refresh</button></span>
          <span> { this.state.loading_data }</span>
      </div>
    );
  }
}

Delete.contextType = TableDataContext
Payments.contextType = TableDataContext
export { Delete, Payments };
