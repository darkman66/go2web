import React, { Component } from "react";
import { TableDataContext }  from "./DataProvider";


export const FormErrors = ({formErrors, elem}) =>
  <div className='formErrors'>
    {Object.keys(formErrors).map((fieldName, i) => {
      if(formErrors[fieldName].length > 0 && elem == fieldName){
        return (
          <div key={i+elem} className="alert alert-danger">{fieldName} {formErrors[fieldName]}</div>
        )
      } else {
        return '';
      }
    })}
  </div>

class FormData  extends Component {
    constructor(props) {
        super(props);
        this.state = {
            formErrors: {
                amount: '',
                currency: '',
                endToEndReference: '',
                numericReference: '',
                paymentId: '',
                paymentPurpose: '',
                paymentScheme: '',
                paymentType: '',
                processingDate: '',
                reference: '',
                schemePaymentSubype: '',
                schemePaymentType: '',
            },
            fields: {
                amount: false,
                currency: false,
                endToEndReference: false,
                numericReference: false,
                paymentId: false,
                paymentPurpose: false,
                paymentScheme: false,
                paymentType: false,
                processingDate: false,
                reference: false,
                schemePaymentSubype: false,
                schemePaymentType: false,
            },
            formValid: false,
            formFields: {}
        }
        this.activatePost = this.activatePost.bind(this);
        this.validateField = this.validateField.bind(this);
        this.closeAction = this.closeAction.bind(this);
    }

    handleInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState( () => { this.validateField(name, value) } );
      }

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let defaultValid = ''
        let currencyValid = ''
        let amountValid = ''
        let processingDateValid = ''
        this.state.formFields[fieldName] = value

        switch(fieldName) {
            case 'currency':
                currencyValid = value.match(/^([\w]{3})$/i) ? true : false
                fieldValidationErrors[fieldName] = currencyValid ? '' : ' is invalid, must be example. USD';
                this.state.fields[fieldName] = currencyValid
                break;
            case 'amount':
                amountValid = value.match(/^([0-9\.]+)$/i) ? true : false
                fieldValidationErrors[fieldName] = amountValid ? '' : ' is invalid, must be float';
                this.state.fields[fieldName] = amountValid
                this.state.formFields[fieldName] = parseInt(value)
                break;
            case 'processingDate':
                processingDateValid = value.match(/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/i) ? true : false
                fieldValidationErrors[fieldName] = processingDateValid ? '' : ' is invalid, must be date, ie. YYYY-MM-DD';
                this.state.fields[fieldName] = processingDateValid
                this.state.formFields[fieldName] = value + "T00:00:00Z"// to simplify input field
                break;
            default:
                defaultValid = value.length >= 1;
                fieldValidationErrors[fieldName] = defaultValid ? '': ' is too short';
                this.state.fields[fieldName] = defaultValid
                break;
        }

        this.setState({ formErrors: fieldValidationErrors })

        let form_valid = true
        Object.values(this.state.fields).forEach(item => { !item ? form_valid = false : null });
        this.setState({ formValid: form_valid });
    }

    errorClass(error) {
        return(error.length === 0 ? '' : 'has-error');
    }

    activatePost(e) {
        e.preventDefault()
        // const data = new FormData(e.target);
        const requestOptions = {
            method: 'POST',
            headers: {
                "Accept": 'application/json',
                "Content-Type": 'application/json'
            },
            body: JSON.stringify(this.state.formFields)
        }

        fetch(this.props.endpoint, requestOptions)
        .then(response => {
            if (response.status !== 200) {
              alert("adding failed!")
            }
            return response.json();
        })
        .then(data => this.context.loadTransactions());
    }

    closeAction() {
        this.props.closeAction()
    }

    hasError(field) {
        return validation.errors[field].length > 0
    }

    render() {
      return (
          <div className="modal">
            <div className="modal-content">
                <span className="close" onClick={ this.closeAction } >&times;</span>
                <p>Add record</p>
                <form className="demoForm" onSubmit={this.activatePost}>
                    {Object.entries(this.state.fields).map(el =>
                    <div key={ el[0] }>
                        <div>
                          <FormErrors formErrors={this.state.formErrors} elem={ el[0] } />
                        </div>
                        <div  className={`form-group ${ this.errorClass(el[0]) }`}>
                          <label htmlFor={ el[0] }>{ el[0] }</label>
                          <input type="text" required className="form-control" name={ el[0] } id={ el[0] }
                            onChange={this.handleInput}  />
                        </div>
                    </div>
                    )}

                    <button type="submit" className="btn btn-primary" disabled={ !this.state.formValid }>Save</button>
              </form>
            </div>
        </div>
      );
    }
}


class FormEditor extends React.Component {
    state = {
        showForm: false
    };

    constructor(props) {
        super(props);
        this.openForm = this.openForm.bind(this);
        this.closeAction = this.closeAction.bind(this);
      }

    openForm() {
        return this.setState({ showForm: true });
    }

    closeAction() {
        return this.setState({ showForm: false });
    }

    render() {
        return (
            <div>
                 { this.state.showForm ? <FormData closeAction={ this.closeAction } endpoint={ this.props.endpoint } /> : null }
                <button className="btn btn-primary btn-sm" onClick={ this.openForm } >Add item</button>
            </div>
        );
    }
}

export { FormEditor };
