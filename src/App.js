import React from "react";
import ReactDOM from "react-dom";
import { DataProvider }  from "./DataProvider";
import Table from "./Table";
import { Payments } from "./Payments";
import { FormEditor } from "./FormEditor";


const App = () => (
    <div>
        <div className="container-fluid">
            <Payments endpoint="/api/payments/fetch/"/>
            <FormEditor endpoint="/api/payments/attribute/" />
        </div>
        <DataProvider endpoint="/api/payments/attribute/" render={data => <Table data={data} endpoint="/api/payments/attribute"/>} />
    </div>
);
const wrapper = document.getElementById("app");
// wrapper ? ReactDOM.render(<App />, wrapper) : null;
ReactDOM.render(
  <App />,
  document.getElementById('app')
);
