package tests

import (
	// "bytes"
	"net/http"
	"net/http/httptest"
	// "net/url"
	"testing"
	"encoding/json"
	"gitlab.com/darkman66/go2web/models"
	"github.com/astaxie/beego"
	. "github.com/smartystreets/goconvey/convey"
)

/* API endpoint tests /api/payments/attribute/<:payment_hash>/sponsor_party/ */
func TestValidSponsorePartyRecordsListView(t *testing.T) {
	r, _ := http.NewRequest("GET", "/api/payments/attribute/216d4da9-e59a-4cc6-8df3-3da6e7580b77/sponsor_party/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 200", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
	})
	// load json
	var result []*models.DebtorBeneficiaryPartyRecord
    json.Unmarshal([]byte(w.Body.String()), &result)
    Convey("Subject: Test JSON body in response\n", t, func() {
	    Convey("Result AccountName", func() {
			So(result[0].AccountName, ShouldEqual, "W Owens")
		})
		Convey("Result AccountNumber", func() {
			So(result[0].AccountNumber, ShouldEqual, "31926819")
		})
		Convey("Result AccountNumberCode", func() {
			So(result[0].AccountNumberCode, ShouldEqual, "BBAN")
		})
		Convey("Result AccountType", func() {
			So(result[0].AccountType, ShouldEqual, 0)
		})
		Convey("Result Address", func() {
			So(result[0].Address, ShouldEqual, "1 The Beneficiary Localtown SE2")
		})
		Convey("Result BankID", func() {
			So(result[0].BankID, ShouldEqual, "403000")
		})

	})
}

// record does not exist
func TestValidSponsorePartyNotExistListView(t *testing.T) {
	r, _ := http.NewRequest("GET", "/api/payments/attribute/7eb8277a-6c91/sponsor_party/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 404", func() {
			So(w.Code, ShouldEqual, 404)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldEqual, 0)
		})
	})
}
