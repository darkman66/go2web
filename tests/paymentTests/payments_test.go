package tests

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	// "net/url"
	"testing"
	"encoding/json"
	"gitlab.com/darkman66/go2web/models"
	"github.com/astaxie/beego"
	. "github.com/smartystreets/goconvey/convey"
)

/* API endpoint tests /api/payments/attribute/ */
func TestValidRecordsListView(t *testing.T) {
	r, _ := http.NewRequest("GET", "/api/payments/attribute/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 200", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
	})
	// load json
	var result []models.PaymentAttribute
    json.Unmarshal([]byte(w.Body.String()), &result)
    Convey("Subject: Test JSON body in response\n", t, func() {
	    Convey("Result is actual JSON", func() {
			So(result[0].Amount, ShouldEqual, "100.21")
		})
		Convey("Result is actual JSON and field currency check", func() {
			So(result[0].Currency, ShouldEqual, "GBP")
		})
	})
}


/* API endpoint tests /api/payments/attribute/<resource uuid>/ */
func TestFailsPostMethodNotAllowed(t *testing.T) {
	r, _ := http.NewRequest("POST", "/api/payments/attribute/4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 40", func() {
			So(w.Code, ShouldEqual, 405)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
	})
}


func TestFailsEmptyPayload(t *testing.T) {
	r, _ := http.NewRequest("POST", "/api/payments/attribute/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Empty JSON\n", t, func() {
		Convey("Status Code Should Be 405", func() {
			So(w.Code, ShouldEqual, 400)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
		Convey("Check expected error in response", func() {
			type Response struct {
				Status bool
				Errors interface{}
				Message string
			}
			var result *Response
			json.Unmarshal([]byte(w.Body.String()), &result)
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
			So(result.Status, ShouldEqual, false)
			So(result.Message, ShouldEqual, "JSON is empty!")
			So(result.Errors, ShouldEqual, nil)
		})
	})
}


func TestFailsBrokenPayload(t *testing.T) {
	r, _ := http.NewRequest("POST", "/api/payments/attribute/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Empty JSON\n", t, func() {
		Convey("Status Code Should Be 405", func() {
			So(w.Code, ShouldEqual, 400)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
		Convey("Check expected error in response", func() {
			type Response struct {
				Status bool
				Errors interface{}
				Message string
			}
			var result *Response
			json.Unmarshal([]byte(w.Body.String()), &result)
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
			So(result.Status, ShouldEqual, false)
			So(result.Message, ShouldEqual, "JSON is empty!")
			So(result.Errors, ShouldEqual, nil)
		})
	})
}


func TestValidPayload(t *testing.T) {
	json_data := []byte(`{"currency": "ABC",
	"amount": "3487",
	"endToEndReference":"iuyt1",
	"numericReference":"iuy",
	"paymentId":"payment id 1","paymentPurpose":"iuy",
	"paymentScheme":"32432","paymentType":"2324324",
	"processingDate":"2011-12-01T00:00:00Z",
	"reference":"reference 1",
	"schemePaymentSubype":"ert",
	"schemePaymentType":"credit"}`)

	r, _ := http.NewRequest("POST", "/api/payments/attribute/", bytes.NewReader(json_data))
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Empty JSON\n", t, func() {
		Convey("Status Code Should Be 405", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("The Result Should Not Be Empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
		Convey("Check expected data in response", func() {
			var result *models.PaymentRecord
			json.Unmarshal([]byte(w.Body.String()), &result)
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
			So(result.Amount, ShouldEqual, "3487")
			So(result.Currency, ShouldEqual, "ABC")
			So(result.EndToEndReference, ShouldEqual, "iuyt1")
			So(result.NumericReference, ShouldEqual, "iuy")
			So(result.Reference, ShouldEqual, "reference 1")
			So(result.SchemePaymentType, ShouldEqual, "credit")
		})
	})
}

func TestValidMethodGet(t *testing.T) {
	r, _ := http.NewRequest("GET", "/api/payments/attribute/4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 200", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("Response is not empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
	})
}

func TestValidMethodGetDetails(t *testing.T) {
	r, _ := http.NewRequest("GET", "/api/payments/attribute/216d4da9-e59a-4cc6-8df3-3da6e7580b77/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 200", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("Response is not empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
		Convey("Check expected data in response", func() {
			var result *models.PaymentRecord
			json.Unmarshal([]byte(w.Body.String()), &result)
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
			So(result.Amount, ShouldEqual, "100.21")
			So(result.Currency, ShouldEqual, "GBP")
			So(result.EndToEndReference, ShouldEqual, "Wil piano Jan")
			So(result.NumericReference, ShouldEqual, "1002001")
			So(result.Reference, ShouldEqual, "Payment for Em's piano lessons")
			So(result.SchemePaymentType, ShouldEqual, "ImmediatePayment")
		})
	})
}

func TestValidMethodDelete(t *testing.T) {
	r, _ := http.NewRequest("DELETE", "/api/payments/attribute/4ee3a8d8-ca7b-4290-a52c-dd5b6165ec43/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 200", func() {
			So(w.Code, ShouldEqual, 200)
		})
		Convey("Response is not empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
	})
}

func TestFailsMethodDelete(t *testing.T) {
	r, _ := http.NewRequest("DELETE", "/api/payments/attribute/4ee3a8d8-ca7b-4290-a52c-1111/", nil)
	w := httptest.NewRecorder()
	beego.BeeApp.Handlers.ServeHTTP(w, r)
	Convey("Subject: Test Station Endpoint\n", t, func() {
		Convey("Status Code Should Be 404", func() {
			So(w.Code, ShouldEqual, 404)
		})
		Convey("Response is not empty", func() {
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
		})
		Convey("Check expected error in response", func() {
			type Response struct {
				Status bool
				Errors interface{}
				Message string
			}
			var result *Response
			json.Unmarshal([]byte(w.Body.String()), &result)
			So(w.Body.Len(), ShouldBeGreaterThan, 0)
			So(result.Status, ShouldEqual, false)
			So(result.Message, ShouldEqual, "Invalid record id!")
			So(result.Errors, ShouldEqual, nil)
		})
	})
}
