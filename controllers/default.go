package controllers

import (
    "time"
    "github.com/astaxie/beego"
)

/* just a simple main view */
type MainController struct {
    beego.Controller
}


func (c *MainController) Get() {
    c.Data["Website"] = "code4tag.eu"
    c.Data["Email"] = "hubert.piotrowski@hotmail.com"
    c.Data["t"] = time.Now().Unix()
    c.TplName = "index.tpl"
}
