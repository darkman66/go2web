package controllers

import (
    "github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/logs"
	"gitlab.com/darkman66/go2web/models"
)

type PaymentListController struct {
    beego.Controller
}

type MainResponse struct {
	Data []*models.PaymentRecord
	Links map[string]string
}

/*** list view ***/
func (this *PaymentListController) Get() {
	/*
	Main view
	*/
	var payments []*models.PaymentRecord
	var amounts []*models.AmountRecord

    o := orm.NewOrm()
    o.QueryTable("payment_record").RelatedSel().All(&payments)

	for i, value := range payments {
		o.QueryTable("amount").Filter("charges_information_record_id", value.ChargesInformation.ID).All(&amounts)
		payments[i].ChargesInformation.SenderCharges = amounts
	}

	response := MainResponse{
    	Links: map[string]string{"self": "https://gitlab.com/darkman66/go2web/"},
    	Data: payments,
    }
    logs.Info("Record already exists. Id: %v", payments)
    this.Data["json"] = &response
    this.ServeJSON()
}
