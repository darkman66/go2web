package controllers

import (
    "github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"gitlab.com/darkman66/go2web/models"
)

type PaymentSponsoredListController struct {
    beego.Controller
}

type PaymentSponsoredDetailsController struct {
    beego.Controller
}

/*** PaymentSponsoredListController ***/
func (this *PaymentSponsoredListController) Get() {
	/*
	All records to show
	*/
	payment_uuid := this.Ctx.Input.Param(":payment_hash")
	var record []*models.DebtorBeneficiaryPartyRecord
	var payment models.PaymentRecord

    o := orm.NewOrm()

    o.QueryTable("payment_record").Filter("payment_record", payment_uuid).One(&payment)
	if payment.ID == 0 {
		this.Ctx.Output.SetStatus(404)
		return
	}

	o.QueryTable("debtor_beneficiary_party").Filter("id__in", payment.DebtorParty.ID, payment.BeneficiaryParty.ID, ).All(&record)
	if &record == nil {
		this.Ctx.Output.SetStatus(404)
		return
	}
    this.Data["json"] = &record
    this.ServeJSON()
}

/*** details  view ***/
func (this *PaymentSponsoredDetailsController) Get() {
	/*
	All records to show
	*/
	payment_uuid := this.Ctx.Input.Param(":payment_hash")
	resource_id := this.Ctx.Input.Param(":resource_id")
	var record models.DebtorBeneficiaryPartyRecord
	var payment models.PaymentRecord

    o := orm.NewOrm()

    qs := o.QueryTable("payment_record").Filter("payment_record", payment_uuid)
	qs.One(&payment)

	if payment.ID == 0 {
		this.Ctx.Output.SetStatus(404)
		return
	}

	o.QueryTable("debtor_beneficiary_party").Filter("id", resource_id).One(&record)
	if record.ID != 0 {
		this.Data["json"] = &record
	} else {
		this.Ctx.Output.SetStatus(404)
		return
	}
    this.ServeJSON()
}

func (this *PaymentSponsoredDetailsController) Delete() {

	payment_uuid := this.Ctx.Input.Param(":payment_hash")
	resource_id := this.Ctx.Input.Param(":resource_id")
	var record models.DebtorBeneficiaryPartyRecord
	var payment models.PaymentRecord
	responseJson := Response{Status: true}
    o := orm.NewOrm()

    o.QueryTable("payment_record").Filter("payment_record", payment_uuid).One(&payment)
	if payment.ID == 0 {
		this.Ctx.Output.SetStatus(404)
		return
	}

	o.QueryTable("debtor_beneficiary_party").Filter("id", resource_id).One(&record)
	if record.ID > 0 {
		_, err := o.Delete(&models.DebtorBeneficiaryPartyRecord{ID: record.ID})
		if err != nil  {
			responseJson.Status = false
			responseJson.Errors = err
		}
	} else {
		this.Ctx.Output.SetStatus(400)
		responseJson.Status = false
		responseJson.Message = "Invalid record id!"
	}

	this.Data["json"] = &responseJson
    this.ServeJSON()
}
