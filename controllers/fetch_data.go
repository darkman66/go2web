package controllers

import (
    "net/http"
    "io/ioutil"
    "time"
    "encoding/json"
    "github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/logs"
	"github.com/araddon/dateparse"
	"gitlab.com/darkman66/go2web/models"
)

type FetchData struct {
    beego.Controller
}

// error checker
func check(e error) {
    if e != nil {
        logs.Error(e)
    }
}


// func fetch_attribute_record(payment_record string, payment_type string, payment_scheme string, payment_id string) *models.PaymentRecord {
// 	o := orm.NewOrm()
// 	payment := models.PaymentRecord{
// 		PaymentRecord: payment_record
// 	}
// 	err := o.Read(&payment)

// 	if err == orm.ErrNoRows {
// 		logs.Info("No record.")
// 	    return nil
// 	} else if err == orm.ErrMissPK {
// 	    logs.Warning("No primary key found.")
// 	    return nil
// 	} else {
// 		logs.Info("Already exists: %v", payment.ID)
// 	    return &payment
// 	}
// }

func ChargesAmount(item *models.ChargesInformationRecord, attr models.Amount) models.AmountRecord {
	o := orm.NewOrm()
	data := models.AmountRecord{
		Amount: attr.Amount,
	    Currency: attr.Currency,
	    ChargesInformationRecordID: item.ID,
	}
	o.ReadOrCreate(&data, "Amount", "Currency")
	return data
}


func ChargesInformationItem(attr models.ChargesInformation) models.ChargesInformationRecord {
	o := orm.NewOrm()

	data := models.ChargesInformationRecord{
		BearerCode: attr.BearerCode,
	    // SenderCharges: charges,
	    ReceiverChargesAmount: attr.ReceiverChargesAmount,
	    ReceiverChargesCurrency: attr.ReceiverChargesCurrency,
	}
	o.ReadOrCreate(&data, "BearerCode")
	for _, value := range attr.SenderCharges {
		ChargesAmount(&data, value)
	}


	return data
}


func PaymentFxItem(attr models.PaymentFx) models.PaymentFxRecord {
	o := orm.NewOrm()
	data := models.PaymentFxRecord{
		ContractReference: attr.ContractReference,
	    ExchangeRate: attr.ExchangeRate,
	    OriginalAmount: attr.OriginalAmount,
	    OriginalCurrency: attr.OriginalCurrency,
	}
	o.ReadOrCreate(&data, "ContractReference")
	return data
}


func DebtorBeneficiaryPartyItem(attr models.DebtorBeneficiaryParty) models.DebtorBeneficiaryPartyRecord {
	o := orm.NewOrm()
	data := models.DebtorBeneficiaryPartyRecord{
		AccountName: attr.AccountName,
		AccountNumber: attr.AccountNumber,
		AccountNumberCode: attr.AccountNumberCode,
		AccountType: attr.AccountType,
		Address: attr.Address,
		BankID: attr.BankID,
		BankIDCode: attr.BankIDCode,
		Name: attr.Name,
	}
	o.ReadOrCreate(&data, "AccountNumber", "AccountNumberCode")
	return data
}


func saveTodb(payments *models.Payment) int {
	/*
	Save data item to DB
	*/
	o := orm.NewOrm()
	var items = 0

	for _, value := range payments.Data {
		processingDate, err := dateparse.ParseLocal(value.Attributes.ProcessingDate)
		check(err)

		beneficent := DebtorBeneficiaryPartyItem(value.Attributes.BeneficiaryParty)
		debtor := DebtorBeneficiaryPartyItem(value.Attributes.DebtorParty)
		payment_fx := PaymentFxItem(value.Attributes.Fx)
		charges_information := ChargesInformationItem(value.Attributes.ChargesInformation)
		payment := models.PaymentRecord{
		    Amount: value.Attributes.Amount,
		    PaymentRecord: value.ID,
		    Currency: value.Attributes.Currency,
		    EndToEndReference: value.Attributes.EndToEndReference,
		    NumericReference: value.Attributes.NumericReference,
		    PaymentID: value.Attributes.PaymentID,
		    PaymentPurpose: value.Attributes.PaymentPurpose,
		    PaymentScheme: value.Attributes.PaymentScheme,
		    PaymentType: value.Attributes.PaymentType,
		    ProcessingDate: processingDate,
		    Reference: value.Attributes.Reference,
		    SchemePaymentSubype: value.Attributes.SchemePaymentSubype,
		    SchemePaymentType: value.Attributes.SchemePaymentType,
		    BeneficiaryParty: &beneficent,
		    DebtorParty: &debtor,
		    PaymentFx: &payment_fx,
		    ChargesInformation: &charges_information,
		}

		created, id, err := o.ReadOrCreate(&payment, "PaymentID", "PaymentScheme", "PaymentRecord")
		check(err)
	    if created {
	        logs.Info("Inserted record ID: %v\n", id)
			items++
	    } else {
	        logs.Info("Record already exists. Id: %v", payment.ID)
	    }
	}

	logs.Info("Number of items inserted: %v\n", items)
	return items
}


func getPayments(url string) models.Payment {
	/*
	Fetching JSON payment data
	TODO: use http magic, ie. https://github.com/parnurzeal/gorequest
	*/
    var httpClient = &http.Client{Timeout: 10 * time.Second} // we can use timeout this way
    logs.Info("Fetching externa data: %s\n", url)
    r, err := httpClient.Get(url)
    check(err)
    defer r.Body.Close()

    var result models.Payment
    body, err := ioutil.ReadAll(r.Body)
    check(err)
    check(json.Unmarshal(body, &result))
    return result
}


/*** API views ***/
func (this *FetchData) Get() {
	/*
	Fetch JSON payments from external resource
	*/
    data := getPayments("http://mockbin.org/bin/41ca3269-d8c4-4063-9fd5-f306814ff03f")
    type Response map[string]string
    response := Response{"response": "ok"}
    saveTodb(&data)
    this.Data["json"] = &response
    this.ServeJSON()
}
