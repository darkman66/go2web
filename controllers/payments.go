package controllers

import (
	"fmt"
    "github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"github.com/astaxie/beego/logs"
	"github.com/satori/go.uuid"
	"github.com/gookit/validate"
	"gitlab.com/darkman66/go2web/models"
)

type PaymentAttributeListController struct {
    beego.Controller
}

type PaymentAttributeDetailsController struct {
    beego.Controller
}


type Response struct {
	Status bool
	Errors interface{}
	Message string
}


/*** list view ***/
func (this *PaymentAttributeListController) Get() {
	/*
	All records to show
	*/
    o := orm.NewOrm()
	var payments []*models.PaymentRecord
	o.QueryTable("payment_record").All(&payments)
    this.Data["json"] = &payments
    this.ServeJSON()
}


func (this *PaymentAttributeListController) Post() {
	/*
	Create a single record
	*/
	responseJson := Response{Status: true}

	if len(this.Ctx.Input.RequestBody) == 0 {
		responseJson.Status = false
		responseJson.Message = "JSON is empty!"
		this.Ctx.Output.SetStatus(400)
		this.Data["json"] = &responseJson
	    this.ServeJSON()
	    return
	}

	PaymentRecordValidator := map[string]string{
		"amount": "required|minLen:1",
	    "currency": "required|minLen:3",
	    "endToEndReference": "required|minLen:1",
	    "numericReference": "required|minLen:1",
	    "paymentId": "required|minLen:1",
	    "paymentPurpose": "required|minLen:1",
	    "paymentScheme": "required|minLen:1",
	    "paymentType": "required|minLen:1",
	    "processingDate": "required",
	    "reference": "required|minLen:1",
	    "schemePaymentSubype": "required|minLen:1",
	    "schemePaymentType": "required|minLen:1",
	}

	d, err := validate.FromJSONBytes(this.Ctx.Input.RequestBody)
	if err != nil {
		responseJson.Status = false
		responseJson.Message = fmt.Sprintf("JSON decode failed: %v", err.Error())
		this.Ctx.Output.SetStatus(400)
	} else {
		payment := models.PaymentRecord{
			PaymentRecord: uuid.Must(uuid.NewV4()).String(),
		}
		v := d.Validation()
		v.StringRules(PaymentRecordValidator)

		if v.Validate() {
			v.BindSafeData(&payment)
			o := orm.NewOrm()
			// i know, over and over you can post same data, which is fine for this excercise
			if created, id, err := o.ReadOrCreate(&payment, "PaymentRecord"); err == nil {
			    if created {
			        this.Data["json"] = &payment
				    this.ServeJSON()
				    return
			    } else {
			        logs.Info("Record already exists. Id:", payment.ID)
					responseJson.Status = false
					responseJson.Message = fmt.Sprintf("Record already exists: %v", id)
					this.Ctx.Output.SetStatus(400)
			    }
			}
		} else {
			responseJson.Status = false
			responseJson.Errors = v.Errors
			logs.Error("Failed validation: %v\n", responseJson.Errors)
			this.Ctx.Output.SetStatus(400)
		}
	}
    this.Data["json"] = &responseJson
    this.ServeJSON()
}

/**** details view ***/
func (this *PaymentAttributeDetailsController) Get() {
	/*
	Get payment record details
	*/
	var payment models.PaymentRecord
	o := orm.NewOrm()
	o.QueryTable("payment_record").Filter("payment_record", this.Ctx.Input.Param(":payment_hash")).One(&payment)
	if payment.ID != 0 {
		this.Data["json"] = &payment
	} else {
		this.Ctx.Output.SetStatus(404)
	}
    this.ServeJSON()
}

func (this *PaymentAttributeDetailsController) Delete() {
	/*
	Delete payment record
	*/
	var payment models.PaymentRecord

	o := orm.NewOrm()
	payment_uuid := this.Ctx.Input.Param(":payment_hash")
	logs.Info("Trying to delete: %v\n", payment_uuid)
	o.QueryTable("payment_record").Filter("payment_record", payment_uuid).One(&payment)
	responseJson := Response{Status: true}

	if payment.ID > 0 {
		_, err := o.Delete(&models.PaymentRecord{ID: payment.ID})
		if err != nil  {
			responseJson.Status = false
			responseJson.Errors = err
		}
	} else {
		this.Ctx.Output.SetStatus(404)
		responseJson.Status = false
		responseJson.Message = "Invalid record id!"
	}


	this.Data["json"] = &responseJson
    this.ServeJSON()
}
