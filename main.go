package main

import (
	"os"
	"fmt"
	_ "gitlab.com/darkman66/go2web/routers"
	"github.com/astaxie/beego"
    "github.com/astaxie/beego/orm"
    "github.com/astaxie/beego/logs"
     _ "github.com/go-sql-driver/mysql"
)


func getenv(key, defaultValue string) string {
    value := os.Getenv(key)
    if len(value) == 0 {
        return defaultValue
    }
    return value
}

func main() {
	mysqlPassword := getenv("MYSQL_PASSWORD", "") // for gitlab only :)
	mysqlUser := getenv("MYSQL_USER", "root")
	mysqlDb := getenv("MYSQL_DB", "nee")

	var connectionString string
    orm.RegisterDriver("mysql", orm.DRMySQL)
    connectionString = fmt.Sprintf("%s:%s@/%s?charset=utf8", mysqlUser, mysqlPassword, mysqlDb)
    logs.Info("Using connection string: %s", connectionString)
    orm.RegisterDataBase("default", "mysql", connectionString)
	beego.Run()
}
