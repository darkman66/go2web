CREATE TABLE IF NOT EXISTS `payment_record`(
     `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `payment_record` varchar(100),
    `amount` varchar(255),
    `currency` varchar(10),
    `end_to_end_reference` varchar(100),
    `numeric_reference` varchar(50),
    `payment_id` varchar(50),
    `payment_purpose` varchar(50),
    `payment_scheme` varchar(50),
    `payment_type` varchar(50),
    `processing_date` date,
    `reference` varchar(50),
    `scheme_payment_subype` varchar(50),
    `scheme_payment_type` varchar(50)
);

CREATE UNIQUE INDEX IF NOT EXISTS "payment_record_idx" ON "payment_record" ("payment_record", "payment_type", "payment_scheme", "payment_id");
